tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;


expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
	      | ^(PLUS  i1=ID   e2=expr) {$out = memory.get($i1.text) + $e2.out;}
	      // Z jakiegoś pwodu nie mogę stworzyć variantu ID po prawej stronie
	      //| ^(PLUS  e1=expr i2=ID)   {$out = $e1.out + memory.get($i2.text);} 
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MINUS i1=ID   e2=expr) {$out = memory.get($i1.text) - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(MUL   i1=ID   e2=expr) {$out = memory.get($i1.text) * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(DIV   i1=ID   e2=expr) {$out = memory.get($i1.text) / $e2.out;}
        | ^(PODST i1=ID   e2=expr) {$out = $e2.out; memory.put($i1.text, $e2.out);}
        | ^(POT   e1=expr e2=expr) {$out = (int) Math.pow($e1.out, $e2.out);}
        | ^(POT   i1=ID   e2=expr) {$out = (int) Math.pow(memory.get($i1.text), $e2.out);}
        | ^(VAR   i1=ID   e2=expr) {$out = $e2.out; memory.put($i1.text, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        ;
